import React from 'react';

class UserRow extends React.Component {

  render() {
    return (
      <li onClick={this.props.onClick} className={this.props.connected ? 'collection-item active' : 'collection-item' }>
          {this.props.username}
      </li>
    )
  }
}

export default UserRow;