import { RECEIVE_LOGIN, REQUEST_LOGIN, RECEIVE_REGISTER, REQUEST_REGISTER, LOGOUT } from '../constants/auth';

const initialState = {
  token: null,
  userId: null,
  isAdmin: false,
  isAuthenticated: false,
  isAuthenticating: false,
  isRegistering: false,
  statusText: ''
};

export function auth(state = initialState, action) {
  switch (action.type){

    case REQUEST_LOGIN: {
      return Object.assign({}, state, {
        isAuthenticating: true
      });
    }

    case RECEIVE_LOGIN: {
      return Object.assign({}, state, {
        isAuthenticating: false,
        isAuthenticated: true,
        token: action.token,
        userId: action.userId,
        isAdmin: action.isAdmin
      });
    }

    case LOGOUT: {
      return Object.assign({}, state, {
        isAuthenticated: false,
        token: null,
        isAdmin: false
      });
    }

    case REQUEST_REGISTER: {
      return Object.assign({}, state, {
        isRegistering: true
      });
    }

    case RECEIVE_REGISTER: {
      return Object.assign({}, state, {
        isRegistering: false,
        isAuthenticated: true,
        token: action.token,
        userId: action.userId,
        isAdmin: action.isAdmin
      });
    }

    default:
      return state;
  }
}
