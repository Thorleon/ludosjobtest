import React,  { PropTypes }  from 'react';

const containerPropTypes = {
    auth: React.PropTypes.shape({
      isAdmin: React.PropTypes.bool.isRequired,
      isAuthenticated: React.PropTypes.bool.isRequired,
      token: React.PropTypes.string
    }),
    users: React.PropTypes.shape({
      users: React.PropTypes.array,
      connections : React.PropTypes.array
    })
};

export default containerPropTypes;