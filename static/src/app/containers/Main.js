import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';

import * as authActions from '../actions/authActions';

import propTypes from '../constants/containersPropTypes';

class Main extends React.Component {

  constructor(props) {
    super(props);
  }

  onLogout() {
      const { dispatch } = this.props;
      dispatch(authActions.logout());
      browserHistory.push('/');
  }

  render() {
    return (
      <section className="page">
        <header>
            <nav>
              {!this.props.auth.isAuthenticated ?
                <div>
                  <Link to="/">Home</Link>
                  <Link to="/login">Login</Link>
                  <Link to="/register">Register</Link>
                </div> :
                  <div>
                    <Link to="/">Home</Link>
                    <Link to="/list">List</Link>
                      {this.props.auth.isAdmin ?
                        <Link to="/connections">Connections</Link> :
                          null
                      }
                    <a href="javascript:void(0)" onClick={ this.onLogout.bind(this) }>Logout</a>
                  </div>
              }
            </nav>
            {this.props.auth.statusText}
        </header>
        {this.props.children}
      </section>
    )
  }
}

Main.propTypes = propTypes;

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(mapStateToProps)(Main);
