import React from 'react';

class Home extends React.Component {

  render() {
    return (
      <section className="home">
        <h1>Home Page</h1>
        <article>
            <h4>Quick specification:</h4>
            <ul>
                <li>User can login or register on relative subpage</li>
                <li>After authentication there is list of other users(without self and admins)</li>
                <li>User can create connection by click on other user</li>
                <li>Admin credentials: username: admin, password: password</li>
                <li>Admin can check user list, but can't create connections</li>
                <li>In connection tab admin can check list of connections</li>
            </ul>
        </article>
      </section>
    )
  }
}

export default Home;

