import React from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';

import * as authActions from '../actions/authActions';

import propTypes from '../constants/containersPropTypes';

import axios from 'axios';

const mapStateToProps = state => ({
    auth: state.auth
});

export function NoAuthRequired(Component) {

    class noAuthenticatedComponent extends React.Component {

        constructor(props) {
            super(props);
        }

        componentWillMount() {
            this.checkAuth();
        }

        componentWillReceiveProps(nextProps) {
            this.checkAuth(nextProps);
        }

        checkAuth(props = this.props) {
            if (props.isAuthenticated) {
                browserHistory.push('/list');
            } else {
                const token = localStorage.getItem('token');
                if (token) {
                    axios.post('api/verify', {
                        token
                    }).then(res => {
                        if (res.status === 200) {
                            const { dispatch } = this.props;
                            dispatch(authActions.receiveLogin(res.data));
                            browserHistory.push('/list');
                        }
                    });
                }
            }
        }

        render() {
            return (
                <div>
                    { !this.props.isAuthenticated
                        ? <Component {...this.props} />
                        : null
                    }
                </div>
            );

        }
    }

    noAuthenticatedComponent.propTypes = propTypes;

    return connect(mapStateToProps)(noAuthenticatedComponent);
}

