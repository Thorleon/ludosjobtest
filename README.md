# Goals: #
## Problem: ##
We have a number of users that we need to keep track of, we also wish to keep track of the connections between users.

## Your solution: ##
We would like you to build a web app or service that allows:
*  Multiple users to register,
*  Registered users to search for other users, but not see who the users are connected to,
*  Registered users to form a connection with alternative users,
*  An admin user to see a list of users with connections.

## Restrictions: ##
*  You may use any language, library and framework of your choice,
*  Consider that your solution should come with deployment instructions.

Please provide your solution in a compressed file.

**Deployment instructions in repo**